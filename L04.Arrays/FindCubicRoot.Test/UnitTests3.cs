using NUnit.Framework;
using static FindCubicRoot.Root;

namespace FindCubicRoot.Test
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void FindRoot3ThrowsExceptionWhenGivenNumberIsNegative()
        {
            Assert.Throws<ArgumentException>(() => FindRoot3(-1));
        }

        [Test]
        public void FindRoot3ReturnsZeroWhenGivenNumberIsZero()
        {
            Assert.That(FindRoot3(0), Is.EqualTo(0));
        }

        [Test]
        public void FindRoot3Returns1WhenGivenNumberIs1()
        {
            Assert.That(FindRoot3(1), Is.EqualTo(1));
        }

        [Test]
        public void FindRoot3WorksCorrectWhenGivenNumberIs8()
        {
            Assert.That(FindRoot3(8), Is.EqualTo(2));
        }

        [Test]
        public void FindRoot3WorksCorrectWithBigNumber()
        {
            Assert.That(FindRoot3(2146689000), Is.EqualTo(1290));
        }

        // Another decision tests
        [Test]
        public void Root3ThrowsExceptionWhenGivenNumberIsNegative()
        {
            Assert.Throws<ArgumentException>(() => Root3(-1));
        }

        [Test]
        public void Root3ReturnsZeroWhenGivenNumberIsZero()
        {
            Assert.That(Root3(0), Is.EqualTo(0));
        }

        [Test]
        public void Root3Returns1WhenGivenNumberIs1()
        {
            Assert.That(Root3(1), Is.EqualTo(1));
        }

        [Test]
        public void Root3WorksCorrectWhenGivenNumberIs8()
        {
            Assert.That(Root3(8), Is.EqualTo(2));
        }

        [Test]
        public void Root3WorksCorrectWithBigNumber()
        {
            Assert.That(Root3(2146689000), Is.EqualTo(1290));
        }
    }
}
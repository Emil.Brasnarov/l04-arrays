using NUnit.Framework;
using static FindMissingNumber.MissingNumber;

namespace FindMissingNumber.Test
{
    [TestFixture]
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void FindMissingThrowsExceptionWhenArrayLengthIsLessOrEqualTo1()
        {
            int[] arr = { 1 };

            Assert.Throws<ArgumentException>(() => FindMissing(arr));
        }

        [Test]
        public void FindMissingWorksCorrectWith2Numbers()
        {
            int[] arr = { 1, 3 };

            Assert.That(FindMissing(arr), Is.EqualTo(2));
        }

        [Test]
        public void FindMissingWorksCorrectWithMoreNumbers()
        {
            int[] arr = { 3, 1, 4 };

            Assert.That(FindMissing(arr), Is.EqualTo(2));
        }
    }
}